const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const del = require('del');

gulp.task('styles', () => {
    return gulp.src('src/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./src/css/'));
});

// gulp.task('js', () => {
//     return gulp.src([
//         'src/js/*.js'
//     ])
//         .pipe(concat('main.js', {newLine: ';'}).on('error', sass.logError))
//         .pipe(gulp.dest('./src/dist/'));
// });

gulp.task('clean', () => {
    return del([
        'src/css/main.css',
    ]);
});

gulp.task('default', gulp.series(['clean', 'styles']));

gulp.task('watch', () => {
    gulp.watch('src/sass/**/*.scss', (done) => {
        gulp.series(['clean', 'styles'])(done);
    });
    // gulp.watch('src/js/**/*.js', (done) => {
    //     gulp.series(['js'])(done);
    // });
});