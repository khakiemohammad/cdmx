import { gsap } from "gsap";

import { CSSRulePlugin } from "gsap/CSSRulePlugin";

gsap.registerPlugin(CSSRulePlugin);

const tags = gsap.timeline({paused:true})
        .to(".shadow", 0.6, {yPercent: -100})
        .to(".showcase", 0.6, {y: 300},0)
        .to(".search-fold", 0.6, {height: 472},0)
        .to(".swipe", 0.6, {opacity:0,display: "none"},0)
        .to(".before", 0.1, {opacity: 1},0)
        .to(".after", 0.6, {opacity: 1},0)
        .to(".tags", 0.6, {opacity:1, display:"block"},0)
        .to(".discover", 0.6, {opacity:1, display:"block"},0)

const menu = gsap.timeline({paused:true})
        .to(".navbar",0.6,{xPercent:100},0)
        .to(".home",0.6,{opacity:0.5,scale:0.75},0)

window.onscroll= function(){
    const percent = window.scrollY / 300;
    const sec = percent * 0.6;
    tags.seek(sec);
};

document.getElementById('openMenu').addEventListener('click',function(event){
    event.preventDefault();
    menu.play();
});

document.getElementById('closeMenu').addEventListener('click',function(event){
    event.preventDefault();
    menu.reverse();
});


const mobiles = gsap.timeline({repeat:-1})
    .to(".mobile-wrapper",0.6,{y:-188},1);